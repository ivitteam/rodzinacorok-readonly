﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace StaticHtmlGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathPhotos = AppDomain.CurrentDomain.BaseDirectory.Replace(@"StaticHtmlGenerator\bin\Debug\", @"ZjazdRodzinny\assets\img\Photos\");
            string pathStaticContent = AppDomain.CurrentDomain.BaseDirectory.Replace(@"StaticHtmlGenerator\bin\Debug\", @"ZjazdRodzinny\StaticViews\");

            var albums = Directory.GetDirectories(pathPhotos);

            foreach (var album in albums)
            {
                var document = GenerateDocForAlbum(album);
                var savePath = $"{pathStaticContent}photo-collection-{album.Split('\\').Last()}.htm";
                document.Save(savePath);
            }
        }

        private static XmlDocument GenerateDocForAlbum(string path)
        {
            XmlDocument doc = new XmlDocument();
            var title = path.Split('\\').Last();

            /*DIV START*/
            XmlNode divOne = doc.CreateElement("div");
            XmlAttribute attr = doc.CreateAttribute("class");
            attr.Value = "bg - faded p - 3 mb - 3 rounded";
            divOne.Attributes?.Append(attr);

            /*DIV HEADER*/
            XmlNode header = doc.CreateElement("h2");
            attr = doc.CreateAttribute("class");
            attr.Value = "text-uppercase mb-3 mt-0 text-slab";
            header.Attributes?.Append(attr);
            header.InnerText = title;
            divOne.AppendChild(header);

            /*DIV ZAWARTOŚCI*/
            XmlNode divContent = doc.CreateElement("div");
            attr = doc.CreateAttribute("class");
            attr.Value = "row";
            divContent.Attributes?.Append(attr);
            attr = doc.CreateAttribute("data-toggle");
            attr.Value = "magnific-popup";
            divContent.Attributes?.Append(attr);
            attr = doc.CreateAttribute("data-magnific-popup-settings");
            attr.Value = @"{""delegate"":""a"", ""gallery"":{""enabled"":true}}""";
            divContent.Attributes?.Append(attr);

            /*SEKCJA FOTO*/
            var i = 0;
            foreach (var filePath in Directory.GetFiles(path))
            {
                if (File.Exists(filePath))
                {
                    var comment = doc.CreateComment($" Photo {i}");
                    divContent.AppendChild(comment);
                    AppendPhoto(i, title, filePath.Split('\\').Last(), ref divContent, ref doc);
                    i++;
                }
            }

            /*DODANIE DO DOKUMENTU*/

            divOne.AppendChild(divContent);
            doc.AppendChild(divOne);

            return doc;
        }

        private static void AppendPhoto(int index, string title, string filename, ref XmlNode contentNode, ref XmlDocument doc)
        {
            XmlNode divPhoto = doc.CreateElement("div");
            XmlAttribute attr = doc.CreateAttribute("class");
            attr.Value = "col-md-6 col-lg-4 col-xl-3 mb-4 rounded";
            divPhoto.Attributes?.Append(attr);
            attr = doc.CreateAttribute("data-animate");
            attr.Value = "fadeIn";
            divPhoto.Attributes?.Append(attr);
            attr = doc.CreateAttribute("data-animate-delay");
            attr.Value = ((double) index / 10).ToString(CultureInfo.InvariantCulture);
            divPhoto.Attributes?.Append(attr);

            XmlNode divA = doc.CreateElement("a");
            attr = doc.CreateAttribute("href");
            attr.Value = $"../assets/img/Photos/{title}/{filename}";
            divA.Attributes?.Append(attr);

            XmlNode divImg = doc.CreateElement("img");
            attr = doc.CreateAttribute("src");
            attr.Value = $"../assets/img/Photos/{title}/{filename}";
            divImg.Attributes?.Append(attr);
            attr = doc.CreateAttribute("class");
            attr.Value = "img-fluid";
            divImg.Attributes?.Append(attr);
            attr = doc.CreateAttribute("alt");
            attr.Value = $"Photo {index}";
            divImg.Attributes?.Append(attr);
            attr = doc.CreateAttribute("style");
            attr.Value = "max-height: 298.172px !important";
            divImg.Attributes?.Append(attr);

            divA.AppendChild(divImg);
            divPhoto.AppendChild(divA);
            contentNode.AppendChild(divPhoto);
        }
    }
}
