﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ZjazdRodzinny.Startup))]
namespace ZjazdRodzinny
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
