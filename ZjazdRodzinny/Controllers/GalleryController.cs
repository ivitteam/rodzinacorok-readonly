﻿using System.IO;
using ZjazdRodzinny.Models;
using System.Web.Mvc;

namespace ZjazdRodzinny.Controllers
{
    [Authorize]
    public class GalleryController : ExceptionController
    {
        public ActionResult Index()
        {
            var directories = Directory.GetDirectories(Server.MapPath(Url.Content("~/assets/img/Photos/")));

            ListGalleryViewModel model = new ListGalleryViewModel();
            model.Titles = new System.Collections.Generic.List<string>();

            foreach (string path in directories)
                model.Titles.Add(path);

            return View(model);
        }

        public ActionResult Video()
        {
            return View();
        }
    }
}