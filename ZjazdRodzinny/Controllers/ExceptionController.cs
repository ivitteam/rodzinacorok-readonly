﻿using System.Web.Mvc;
using ZjazdRodzinny.Models;

namespace ZjazdRodzinny.Controllers
{
    public class ExceptionController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            Error err = new Error(filterContext.Exception, "handledInOnException");
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                context.Errors.Add(err);
                context.SaveChanges();
            }
            
            filterContext.ExceptionHandled = true;

            // Redirect on error:
            filterContext.Result = RedirectToAction("Error", "Error");
        }
    }
}