﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZjazdRodzinny.Controllers
{
    public class ErrorController : ExceptionController
    {
        public ActionResult Index()
        {
            return Redirect("~/Error.htm");
        }
        // GET: Error
        public ActionResult Error()
        {
            return Redirect("~/Error.htm");
        }
    }
}