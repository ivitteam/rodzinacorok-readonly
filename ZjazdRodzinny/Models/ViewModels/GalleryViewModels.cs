﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ZjazdRodzinny.Models
{
    public class GalleryViewModel
    {
        [Required]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Ścieżka")]
        public string Path { get; set; }
    }

    public class ListGalleryViewModel
    {
        [Required]
        [Display(Name = "Tytuły")]
        public List<string> Titles { get; set; }
    }
}