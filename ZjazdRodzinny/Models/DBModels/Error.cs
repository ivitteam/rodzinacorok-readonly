﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ZjazdRodzinny.Models
{
    public class Error
    {
        public Error()
        {

        }

        public Error(Exception exception, string code) : this()
        {
            ExceptionMessage = exception.Message;
            InnerExceptionMessage = exception.InnerException?.Message;
            ExceptionCode = code;
            ExceptionStackTrace = exception.StackTrace;
            Date = DateTime.Now;
            ExceptionSource = exception.Source;
            ExceptionTarget = exception.TargetSite?.Name;
        }
        
        [Key]
        public int Id { get; set; }

        public string ExceptionCode { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string ExceptionSource { get; set; }
        public string InnerExceptionMessage { get; set; }
        public string ExceptionTarget { get; set; }
        public DateTime Date { get; set; }
    }

}
